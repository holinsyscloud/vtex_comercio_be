// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var config =  require('./config');

//var database = require('./services/database');
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var cors = require('cors');
var logger = require('morgan');
  var orm   = require('orm');



 var jwt = require('jsonwebtoken');
 var Base64 = require('js-base64').Base64;
 var secretHiddenKey = 'JzKa0t2ri9v28e5J5J4PkEO88YQkIE0O+ZoYBnUHmHga8/o/IduvU/Tht70iE=';

var multer = require('multer');
var errorHandler = require('errorhandler');


module.exports = app;



function main() {

  app.use(cors());
	app.use(logger('dev'));
  	app.use(bodyParser.json());
  	app.use(bodyParser.urlencoded({ extended: true }));


    app.use(function (req, res, next) {

      next();
    });



  app.use(function (req, res, next) {
      if (req.url != '/HS_Autenticar' && req.url !='/HS_Usuario/recuperarPswd'  && req.url.indexOf('/serv/img')<0 && req.url.indexOf('/Cotizacion/download/')<0 && req.url.indexOf('/Cotizacion/Export/')<0 && req.url.indexOf('/Facturacion/Export/')<0 && req.url.indexOf('/margenCotizacion/Export/')<0){
           if (req.headers && req.headers.authorization) {
              var parts = req.headers.authorization.split(' ');
              if (parts.length == 2) {
                var scheme = parts[0],
                credentials = parts[1];
          
                if (/^Basic$/i.test(scheme)) {
                  var decoded = jwt.decode(Base64.decode(credentials));
                 if (decoded != null    && typeof decoded != 'undefined'){


                        var tokenConstructor =  global.db.models.hs_token;
                        var valRecup = (req.url == '/HS_Usuario/nuevoPswd') ? 1 : 0  
                        tokenConstructor.find({idUsuario: decoded.idUsuario, token: decoded.token, valorRecuperacion: valRecup},function findCB(err, selectedToken){
                     
                     

                        if (err) return res.status(401).json({err: 'BAD ACCESS'});
                         
                        if (typeof selectedToken != 'undefined' &&  selectedToken){
                          if (selectedToken[0]){
                           req.userId = decoded.idUsuario;
                           req.token = decoded.token;
                           req.tenant = selectedToken[0].tenant;
                           req.rolId=selectedToken[0].idRol;
                           next();
                         }else{
                              return res.status(401).json({err: 'BAD ACCESS'});

                         }
                        }else{
                          return res.status(401).json({err: 'BAD ACCESS'});

                        }

                    
                  });

                     }else{
                       return res.status(401).json({err: 'BAD ACCESS'});


                     }
              
                }else{

                      return res.status(401).json({err: 'BAD ACCESS'});

                }

              } else {
                return res.status(401).json({err: 'BAD ACCESS'});

              }
            } else 
                return res.status(401).json({err: 'BAD ACCESS'});



        }else{
          next();
        }

});


   	app.set('port', process.env.PORT || 8051);
	


    require('./app/models')(app);
    require('./app/controller')(app);
    require('./app/routes')(app);



	if ('development' == app.get('env')) {
	  app.use(errorHandler());
	}



  	app.listen(app.get('port'), function(){
  
	});


}


global.db=orm.connect(config.database); 
db.on('connect', function(err) {
  if (err)  console.error('Connection error: ' + err);
    else
    main();
});



