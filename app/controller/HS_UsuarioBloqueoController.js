
var HS_Usuario     = require('../models/HS_Usuario');
var HS_AutenticarController  = require('../controller/HS_AutenticarController');
var jwt = require('jsonwebtoken');
var secretKey = 'Vbxq2mlbGJw8XH+ZoYBnUHmHga8/o/IduvU/Tht70iE=';
module.exports = function () {

return{
	  //acumulamos el numero de loggeos no validos
	   amountIntUser:function(idUsuario, tenantUsuario,cantidadMinBloq)
		{
			var minPrimerBloqueo=5
			var minsegundoBloqueo=10
			var bloquearUser=false
			var cantminBloqueo
			
			HS_UsuarioController().getById(tenantUsuario,idUsuario,function(statusCode, result){

				
				if(result[0].numIntentos<3)
				{

					HS_UsuarioController().actualizarNumIntentos(idUsuario,result[0].numIntentos +1,function(){
						
					})

				}
				if(result[0].numIntentos==2)
				{
					
					switch(cantidadMinBloq) {
					    case null:
					        cantminBloqueo=minPrimerBloqueo
					        break;
					    case minPrimerBloqueo:
					        cantminBloqueo=minsegundoBloqueo
					        break;
					    case minsegundoBloqueo:
					        bloquearUser=true
					        break;				    
					}

					if(bloquearUser)
					{
						HS_UsuarioController().bloqueoUsuario(idUsuario,function(){
						
						})	
					}
					else
					{
						HS_UsuarioController().bloqueoUsuarioTemporal(idUsuario,cantminBloqueo,function(){
						
						})	
					}
				}
			})
		},

		desbloqueoTiempo:function(idUsuario,cb){

			var usuarioConstructor = global.db.models.hs_usuario;
	  	   	usuarioConstructor.get(idUsuario, function (err, obj) {
	  	   		if (err){
			 	  cb(500,{err: err});
			 	}
			 	else
				{
						
					if(obj)
					{
						obj.numIntentos=0;
						obj.estado="H";
					  	obj.save(function(err) {
					   	  if (err)
					   	  { 
					   	   cb(500,{err: err});
					   	  }
					   	  else
					   	  {
									
					    	cb(200,{id:obj.id });		  	  	 
						  }			  	  
						});
							}
					else
						{
							cb(404,{message: 'NO EXISTE USUARIO'});
						}
				}

	  	   	})

		},
		desbloqueoUsuario:function(idUsuario,cb){

		var usuarioConstructor = global.db.models.hs_usuario;
  	   	usuarioConstructor.get(idUsuario, function (err, obj) {
  	   		if (err){
		 	  cb(500,{err: err});
		 	}
		 	else
			{
					
				if(obj)
				{
					obj.hora_bloqueo= null;
					obj.cantidad_min_bloqueado=null;
					obj.numIntentos=0;
					obj.estado="H";
				  	obj.save(function(err) {
				   	  if (err)
				   	  { 
				   	   cb(500,{err: err});
				   	  }
				   	  else
				   	  {
								
				    	cb(200,{id:obj.id });		  	  	 
					  }			  	  
					});
						}
				else
					{
						cb(404,{message: 'NO EXISTE USUARIO'});
					}
			}

  	   	})

	},

}
};

